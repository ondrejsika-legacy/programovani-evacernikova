# Lekce 1 (23. 3. 2015)

### Bash prikazy

vetsina prikazu ma manualovou stranku na

    man <prikaz>

napriklad

    man mkdir

* cd - prepne se do adresare
* mkdir - vytvori novy adresar
* ls - vypise soubory z adresare
* touch - vytvori prazny soubor
* pwd - vypise aktuakni cestu
* rm - maze


### Git

Instalace:

    sudo apt-get install git

Odkazy:

* <https://try.github.io/> - interaktivni vyuka gitu online

Prikazy:

* git init - vytvori git repozitar
* `git ad <file or dir>` - prida soubor do gitu a stagged prostoru
* git commit - vytvori changeset (commit message ve Vimu)
* git commit -m 'commit message' - vytvori changeset (commit message se zadava parametrem)
* git log - vypise seznam changesteu
* git diff - ukaze rozdil oproti poslednimu changesetu


### Vim

* zacit psat -> zmacknout `i`
* ukoncit psaco rezim -> `esc`
* ulozit a zavrit -> `:wq` a enter

